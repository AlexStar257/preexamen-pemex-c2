package com.example.empresasmexicopreexamen;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.empresasmexicopreexamen.Modelo.UsuariosDb;

public class RegisterActivity extends AppCompatActivity {

    private Button btnGuardar, btnSalir;
    private EditText txtNombre,txtCorreo,txtContra, txtReContra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        iniciarComponentes();

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });

    }

    private void iniciarComponentes() {
        btnSalir = findViewById(R.id.btnSalir);
        btnGuardar = findViewById(R.id.btnGuardar);
        txtNombre = findViewById(R.id.txtNombre);
        txtCorreo = findViewById(R.id.txtCorreo);
        txtContra = findViewById(R.id.txtContra);
        txtReContra = findViewById(R.id.txtReContra);
    }

    private void registrar() {
        String nombre = txtNombre.getText().toString();
        String correo = txtCorreo.getText().toString();
        String contrasena = txtContra.getText().toString();
        String reContrasena = txtReContra.getText().toString();

        if (!nombre.isEmpty() && !correo.isEmpty() && !contrasena.isEmpty() && !reContrasena.isEmpty()) {
            if (contrasena.equals(reContrasena)) {
                // Las contraseñas coinciden, proceder con el registro
                Usuario nuevoUsuario = new Usuario(nombre, correo, contrasena);
                UsuariosDb usuariosDb = new UsuariosDb(RegisterActivity.this);
                long resultado = usuariosDb.insertUsuario(nuevoUsuario);

                if (resultado > 0) {
                    mostrarToast("Usuario registrado exitosamente");
                    // Imprimir los datos registrados en la consola de depuración
                    Log.d("Registro", "Nombre: " + nombre);
                    Log.d("Registro", "Correo: " + correo);
                    Log.d("Registro", "Contraseña: " + contrasena);
                    finish();
                } else {
                    mostrarToast("Error al registrar el usuario");
                }
            } else {
                mostrarToast("Las contraseñas no coinciden");
            }
        } else {
            mostrarToast("Por favor, complete todos los campos");
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("PEMEX");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}
